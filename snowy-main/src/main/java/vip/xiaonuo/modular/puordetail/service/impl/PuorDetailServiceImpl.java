/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.puordetail.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.puordetail.entity.PuorDetail;
import vip.xiaonuo.modular.puordetail.enums.PuorDetailExceptionEnum;
import vip.xiaonuo.modular.puordetail.mapper.PuorDetailMapper;
import vip.xiaonuo.modular.puordetail.param.PuorDetailParam;
import vip.xiaonuo.modular.puordetail.service.PuorDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 采购细明service接口实现类
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
@Service
public class PuorDetailServiceImpl extends ServiceImpl<PuorDetailMapper, PuorDetail> implements PuorDetailService {

    @Override
    public PageResult<PuorDetailParam> page(PuorDetailParam puorDetailParam) {
        QueryWrapper<PuorDetailParam> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(puorDetailParam)) {
            // 根据采购订单编号 查询
            if (ObjectUtil.isNotEmpty(puorDetailParam.getPurchaseOrder())) {
                queryWrapper.like("c.code",puorDetailParam.getPurchaseOrder());
            }
            // 根据采购明细编号 查询
            if (ObjectUtil.isNotEmpty(puorDetailParam.getCode())) {
                queryWrapper.like("a.code",puorDetailParam.getCode());
            }
            // 根据产品名称 查询
            if (ObjectUtil.isNotEmpty(puorDetailParam.getProIds())) {
                queryWrapper.inSql("a.pro_id",puorDetailParam.getProIds());
            }
            //根据采购订单到货时间查询
            if (ObjectUtil.isNotEmpty(puorDetailParam.getArrivalTime())){
                queryWrapper.inSql("c.arrival_time",puorDetailParam.getArrivalTime().toString());
            }
            //根据采购订单来源查询
            if (ObjectUtil.isNotEmpty(puorDetailParam.getOrderSource())){
                queryWrapper.eq("c.order_source",puorDetailParam.getOrderSource());
            }

        }
        queryWrapper.orderByDesc("a.create_time");
        return new PageResult<>(this.baseMapper.page(PageFactory.defaultPage(),queryWrapper));
    }

    @Override
    public List<PuorDetail> list(PuorDetailParam puorDetailParam) {
        return this.list();
    }

    @Override
    public void add(PuorDetailParam puorDetailParam) {
        PuorDetail puorDetail = new PuorDetail();
        BeanUtil.copyProperties(puorDetailParam, puorDetail);
        this.save(puorDetail);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<PuorDetailParam> puorDetailParamList) {
        List<Long> deleteList = new ArrayList<>();
        puorDetailParamList.forEach(puorDetailParam -> {
            deleteList.add(puorDetailParam.getId());
        });
        this.removeByIds(deleteList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(PuorDetailParam puorDetailParam) {
        PuorDetail puorDetail = this.queryPuorDetail(puorDetailParam);
        BeanUtil.copyProperties(puorDetailParam, puorDetail);
        this.updateById(puorDetail);
    }

    @Override
    public PuorDetail detail(PuorDetailParam puorDetailParam) {
        return this.queryPuorDetail(puorDetailParam);
    }

    /**
     * 获取采购细明
     *
     * @author jiaxin
     * @date 2022-07-27 15:57:25
     */
    private PuorDetail queryPuorDetail(PuorDetailParam puorDetailParam) {
        PuorDetail puorDetail = this.getById(puorDetailParam.getId());
        if (ObjectUtil.isNull(puorDetail)) {
            throw new ServiceException(PuorDetailExceptionEnum.NOT_EXIST);
        }
        return puorDetail;
    }

    @Override
    public void export(PuorDetailParam puorDetailParam) {
        List<PuorDetail> list = this.list(puorDetailParam);
        PoiUtil.exportExcelWithStream("SnowyPuorDetail.xls", PuorDetail.class, list);
    }

}
