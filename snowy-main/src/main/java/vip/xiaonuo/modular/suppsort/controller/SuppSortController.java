/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppsort.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.DataScope;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.suppsort.param.SuppSortParam;
import vip.xiaonuo.modular.suppsort.service.SuppSortService;

import javax.annotation.Resource;
import java.util.List;

/**
 * 供应商分类控制器
 *
 * @author zjk
 * @date 2022-08-04 13:41:54
 */
@RestController
public class SuppSortController {

    @Resource
    private SuppSortService suppSortService;

    /**
     * 查询供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @GetMapping("/suppSort/page")
    @BusinessLog(title = "供应商分类_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(SuppSortParam suppSortParam) {
        return new SuccessResponseData(suppSortService.page(suppSortParam));
    }

    /**
     * 添加供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @PostMapping("/suppSort/add")
    @BusinessLog(title = "供应商分类_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(SuppSortParam.add.class) SuppSortParam suppSortParam) {
            suppSortService.add(suppSortParam);
        return new SuccessResponseData();
    }

    /**
     * 删除供应商分类，可批量删除
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @PostMapping("/suppSort/delete")
    @BusinessLog(title = "供应商分类_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(SuppSortParam.delete.class) List<SuppSortParam> suppSortParamList) {
            suppSortService.delete(suppSortParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @PostMapping("/suppSort/edit")
    @BusinessLog(title = "供应商分类_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(SuppSortParam.edit.class) SuppSortParam suppSortParam) {
            suppSortService.edit(suppSortParam);
        return new SuccessResponseData();
    }

    /**
     * 查看供应商分类
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @GetMapping("/suppSort/detail")
    @BusinessLog(title = "供应商分类_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(SuppSortParam.detail.class) SuppSortParam suppSortParam) {
        return new SuccessResponseData(suppSortService.detail(suppSortParam));
    }

    /**
     * 供应商分类列表
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @GetMapping("/suppSort/list")
    @BusinessLog(title = "供应商分类_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(SuppSortParam suppSortParam) {
        return new SuccessResponseData(suppSortService.list(suppSortParam));
    }

    /**
     * 导出系统用户
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @GetMapping("/suppSort/export")
    @BusinessLog(title = "供应商分类_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(SuppSortParam suppSortParam) {
        suppSortService.export(suppSortParam);
    }

    /**
     * 获取组织机构树
     *
     * @author zjk
     * @date 2022-08-04 13:41:54
     */
    @Permission
    @DataScope
    @GetMapping("/suppSort/tree")
    @BusinessLog(title = "供应商类型树",opType = LogAnnotionOpTypeEnum.TREE)
    public ResponseData tree(SuppSortParam suppSortParam){
        return new SuccessResponseData(suppSortService.tree(suppSortParam));
    }

}