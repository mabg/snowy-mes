/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppdata.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.suppdata.param.SuppDataParam;
import vip.xiaonuo.modular.suppdata.service.SuppDataService;

import javax.annotation.Resource;
import java.util.List;

/**
 *  供应商资料控制器
 *
 * @author zjk
 * @date 2022-08-04 09:12:05
 */
@RestController
public class SuppDataController {

    @Resource
    private SuppDataService suppDataService;

    /**
     * 查询 供应商资料
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @GetMapping("/suppData/page")
    @BusinessLog(title = " 供应商资料_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(SuppDataParam suppDataParam) {
        return new SuccessResponseData(suppDataService.page(suppDataParam));
    }

    /**
     * 添加 供应商资料
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @PostMapping("/suppData/add")
    @BusinessLog(title = " 供应商资料_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(SuppDataParam.add.class) SuppDataParam suppDataParam) {
            suppDataService.add(suppDataParam);
        return new SuccessResponseData();
    }

    /**
     * 删除 供应商资料，可批量删除
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @PostMapping("/suppData/delete")
    @BusinessLog(title = " 供应商资料_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(SuppDataParam.delete.class) List<SuppDataParam> suppDataParamList) {
            suppDataService.delete(suppDataParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑 供应商资料
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @PostMapping("/suppData/edit")
    @BusinessLog(title = " 供应商资料_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(SuppDataParam.edit.class) SuppDataParam suppDataParam) {
            suppDataService.edit(suppDataParam);
        return new SuccessResponseData();
    }

    /**
     * 查看 供应商资料
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @GetMapping("/suppData/detail")
    @BusinessLog(title = " 供应商资料_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(SuppDataParam.detail.class) SuppDataParam suppDataParam) {
        return new SuccessResponseData(suppDataService.detail(suppDataParam));
    }

    /**
     *  供应商资料列表
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @GetMapping("/suppData/list")
    @BusinessLog(title = " 供应商资料_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(SuppDataParam suppDataParam) {
        return new SuccessResponseData(suppDataService.list(suppDataParam));
    }

    /**
     * 导出系统用户
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    @Permission
    @GetMapping("/suppData/export")
    @BusinessLog(title = " 供应商资料_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(SuppDataParam suppDataParam) {
        suppDataService.export(suppDataParam);
    }

}