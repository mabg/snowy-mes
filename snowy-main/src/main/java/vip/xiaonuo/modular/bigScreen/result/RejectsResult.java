package vip.xiaonuo.modular.bigScreen.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class RejectsResult {
    //时间
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date time;
    //不良品数量
    private Integer rejectsNum;
    //不良品率
    private Double rejectsRate;

}
