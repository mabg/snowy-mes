/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.workorder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.modular.workorder.entity.WorkOrder;
import vip.xiaonuo.modular.workorder.param.WorkOrderParam;
import vip.xiaonuo.modular.workorder.result.WorkOrderResult;

import java.util.List;

/**
 * 工单表service接口
 *
 * @author czw
 * @date 2022-05-25 10:31:14
 */
public interface WorkOrderService extends IService<WorkOrder> {

    /**
     * 查询工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    PageResult<WorkOrderResult> page(WorkOrderParam workOrderParam);

    /**
     * 工单表列表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     * @return
     */
    List<WorkOrderResult> list(WorkOrderParam workOrderParam);

    /**
     * 添加工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    void add(WorkOrderParam workOrderParam);

    /**
     * 删除工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    void delete(List<WorkOrderParam> workOrderParamList);

    /**
     * 编辑工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
    void edit(WorkOrderParam workOrderParam);

    /**
     * 查看工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
     WorkOrder detail(WorkOrderParam workOrderParam);

    /**
     * 导出工单表
     *
     * @author czw
     * @date 2022-05-25 10:31:14
     */
     void export(WorkOrderParam workOrderParam);

}
