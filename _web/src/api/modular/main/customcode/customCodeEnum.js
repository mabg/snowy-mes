const TIMEFORMATDATA = [
  {
    name: '年',
    code: 0
  },
  {
    name: '年月',
    code: 1
  },
  {
    name: '年月日',
    code: 2
  },
  {
    name: '无',
    code: 3
  }
]
export default TIMEFORMATDATA
