import { axios } from '@/utils/request'

/**
 * 查询供应商联系人
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonPage (parameter) {
  return axios({
    url: '/suppPerson/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 供应商联系人列表
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonList (parameter) {
  return axios({
    url: '/suppPerson/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加供应商联系人
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonAdd (parameter) {
  return axios({
    url: '/suppPerson/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑供应商联系人
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonEdit (parameter) {
  return axios({
    url: '/suppPerson/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除供应商联系人
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonDelete (parameter) {
  return axios({
    url: '/suppPerson/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出供应商联系人
 *
 * @author ZJK
 * @date 2022-08-04 17:01:20
 */
export function suppPersonExport (parameter) {
  return axios({
    url: '/suppPerson/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
